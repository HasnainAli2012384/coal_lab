.stack 100h
.model small


.data
 var1 db "Hasnain $"
 var2 dw "Ali$ "
 
 
 .code
 main proc
 mov ax,@data 
 mov ds,ax
 
 mov dl, OFFSET var1
 mov ah, 09h
 int 21h
 
 mov dx, OFFSET var2
 mov ah, 09h
 int 21h
 
 main endp
 end 
   